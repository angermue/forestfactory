#' run.forestfactory
#' @description runs FORMIND using a CMDfile and check run for errors.
#'
#' @param model Character string naming the modelfile  
#' @param parFile Character string naming the PARfile  
#' @seealso \code{\link{makeCMD}}
#' 
#' @export


run.forestfactory <- function(model = "forestfactory.exe"
                              , parFile = "formind_parameters/experiment.par"
                              )
{
  cmdfile <- "ff2.cmd"
  
  makeCMD(cmdfile, model, parFile) 
  
  system(cmdfile)
  
  sterr <- readLines("sterr.txt")
  if(length(grep("ERROR", sterr)) > 0){
    cat(sterr[grep("ERROR", sterr)])
    stop("ERROR in FORMIND model run")
  }else{
    cat("simulation succesfull")
  }
  
  file.remove(cmdfile)
}

